defmodule Bikemap.Repo do
  use Ecto.Repo,
    otp_app: :bikemap,
    adapter: Ecto.Adapters.Postgres
end
