defmodule BikemapWeb.PageController do
  use BikemapWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
